<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
//define( 'DB_NAME', 'blog' );

/** MySQL database username */
//define( 'DB_USER', 'wordpress' );

/** MySQL database password */
//define( 'DB_PASSWORD', 'Imanol730830.' );

/** MySQL hostname */
//define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
//define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
//define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 *
define('AUTH_KEY',         'n#PToP$:G.*<gTxIdBK_1_M|N{9f&>}@!Os>V.YsDi{$ZC#h92,2;Sp/JLz?n&Y#');
define('SECURE_AUTH_KEY',  'D0]GXgA_Vf$QPO0T:NF{l~L&2-ZPR{&jPUMYS]v+E$a66-%@+#9BrA!YOHZnQS#b');
define('LOGGED_IN_KEY',    'H<BG2Z$fd_>sY.-%+P6uASsc+s=tbrywc+i#~@=UWRIIA1+?|}]!HqF2`A#X+h-e');
define('NONCE_KEY',        '3+/<k}a^f+dxC}G^VuD@Rr+5 xGLDt)obIGmTGLC/Vx-~o}mHZj6j[_FT8Bg]dm=');
define('AUTH_SALT',        'SDe`_fhq-u(1mJ7c12wg,ff*dmtc!8Bl`tV74q1bUfowc|D79=Emegj8{{t-@s2n');
define('SECURE_AUTH_SALT', 'Po Lb,:_||h+X]&oJ=HT-RhTV2S(hR#-*2:xb[p=mZH|`z^[t0;2?87lA-f6|A3J');
define('LOGGED_IN_SALT',   'x*eVzJ8K&{mte(7+tH-f4Us`e,^_(|9)4hYVu0L]G >$z#(YT,U{<gFq6Gm#C<hG');
define('NONCE_SALT',       '@MQ~A7RY]E;x{%Cb{y2jV`EzlMMScd7hMf2l)QN+mSY} :vM2sa?1r/`ebUSbv*F');
*/

/**#@-*/

define('DB_NAME', $_SERVER['RDS_DB_NAME']);
define('DB_USER', $_SERVER['RDS_USERNAME']);
define('DB_PASSWORD', $_SERVER['RDS_PASSWORD']);
define('DB_HOST', $_SERVER['RDS_HOSTNAME'] . ':' . $_SERVER['RDS_PORT']);
define('DB_CHARSET', 'utf8');
define('DB_COLLATE', '');
define('AUTH_KEY',         $_SERVER['AUTH_KEY']);
define('SECURE_AUTH_KEY',  $_SERVER['SECURE_AUTH_KEY']);
define('LOGGED_IN_KEY',    $_SERVER['LOGGED_IN_KEY']);
define('NONCE_KEY',        $_SERVER['NONCE_KEY']);
define('AUTH_SALT',        $_SERVER['AUTH_SALT']);
define('SECURE_AUTH_SALT', $_SERVER['SECURE_AUTH_SALT']);
define('LOGGED_IN_SALT',   $_SERVER['LOGGED_IN_SALT']);
define('NONCE_SALT',       $_SERVER['NONCE_SALT']);

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );
define('WP_ALLOW_REPAIR', true);

define('FORCE_SSL_ADMIN', true);
// in some setups HTTP_X_FORWARDED_PROTO might contain 
// a comma-separated list e.g. http,https
// so check for https existence
if (strpos($_SERVER['HTTP_X_FORWARDED_PROTO'], 'https') !== false) {
	$_SERVER['HTTPS']='on';
}
/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
